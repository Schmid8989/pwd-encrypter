# pwd-encrypter

This will enable you to encrypt all the files and folders in the present working and generate a secret key.

# Setup

## Encryption

Clone the fx.py file to your directory

```
$ ./fx.py
```

Will encrpyt all the file and folder in the present working directory and a secret key will be exported to the PWD.


## Decryption

Clone the r_fx.py file to your directory 

Add the secret key to the directory

```
$ ./r_fx.py
```
The secret key will be deleted at the end.

Feel free to make this code better

Use it at your own risk!

:)
