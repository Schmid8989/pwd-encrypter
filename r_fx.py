#!/usr/bin/env python3

import os
from cryptography.fernet import Fernet
from colorama import Fore, Back, Style 
import shutil

def decrypt_file(file_name):
    print(f"[DECRYPTING FILE] {file_name}")
    with open("secret.key","rb") as k:
        key = k.read()
    f = Fernet(key)
    if not(file_name=='secret.key' or file_name=='fx.py' or file_name=='r_fx.py'):
        try:
            with open(file_name,"rb") as fil:
                encrypted_data = fil.read()
            decrypted_data = f.decrypt(encrypted_data)
            with open(file_name,"wb") as fil:
                fil.write(decrypted_data)
        except:
            print(f'[IGNORING]{file_name}')
    else:
        pass

if __name__ == "__main__":
    allf = list(os.walk('.'))
    files = allf[0][2]
    zip_files = []
    for fil in files:
        decrypt_file(fil)
        if fil.endswith('.zip'):
            zip_files.append(fil)
    print("[UNPACKING ZIP FILES]")
    for i in zip_files:
        print(Fore.RED+f"[UNZIPPING FILE] {i}"+Style.RESET_ALL)
        shutil.unpack_archive(i,i[:-4],'zip')
        os.remove(i)
        print(Fore.GREEN+f"[UNZIPPING DONE] {i}"+Style.RESET_ALL)
    os.remove('secret.key')
    print(Fore.CYAN+"[UNPACKING COMPLETED]"+Style.RESET_ALL)